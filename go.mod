module gitee.com/eden-framework/codegen

go 1.16

require (
	gitee.com/eden-framework/reflectx v0.0.3
	github.com/stretchr/testify v1.6.1
	golang.org/x/tools v0.0.0-20201013053347-2db1cd791039
)
